const numberFormats = {
  en: {
    currency: { style: "currency", currency: "USD" }
  },
  es: {
    currency: { style: "currency", currency: "USD", currencyDisplay: "code" }
  }
}
export default numberFormats
