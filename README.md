# Full-Stack Challenge

## Live app url - [https://staytion-checkout.herokuapp.com/](https://staytion-checkout.herokuapp.com/)

## Demo
![app video demo](https://firebasestorage.googleapis.com/v0/b/clubapp-8d007.appspot.com/o/staystore-app.gif?alt=media&token=1b3c8436-2144-4eb3-a183-17f8c6160abd)

## Background
This is a demo app to showcase a basic online store features including checkout , payments, i18n, theming and so forth. 

## Assumptions

 - The app will be used by multiple users at once hence the need to be robust, scalable and support concurrency.
 - Users expect a snappy and responsive UI hence need to optimise loading resources and computations.
- Some users might be using the app on mobile devices so the UI needs to responsive and accommodate different screen size.
- We will use the Component pattern to structure the app so that the code is dry, reusable and maintainable

## Tech Stack
The app is built using a modern technology stack that includes:

 1. **Vue.js** - View Layer
 2. **Vuex** - State Management
 3. **Stripe** - Handle Payments
 4. **Node.js/Express** - Backend Server
 5. **Jest** - Test Runner
 9. **Buefy** - UI Framework


## Quickstart Installation

1. Make sure that you have Node.js v8.15.1 and npm v5 or above installed.

2. Clone this repo using `git clone https://gitlab.com/trapsta/staytion-checkout-app.git`

3. CD into the appropriate directory: `cd staytion-checkout-app`.

4. Run `npm install` in order to install dependencies .

5. Define STRIPE_API_KEY, STRIPE_API_SECRET and CLIENT_URL enviromental variables in your .env file

6. Run `npm run dev` to see the development server at `http://localhost:3000` .


## Challenges

 1. Handling multiple currencies and internationalization.
 
2.  Implementing sorting and filters - It was hard to decide where exactly to implement sorting and filters since I didn't want to put this logic in the components since this not presentational logic, I also did not want to store the filtered state in the store since this would either cause duplicated data or I would need to refetch the data from the api every time the user needed to filter by a new category. I the end, I decided to save the sort key and the filter category in the component and use those and a store getter to compute the filtered/sorted data in the computed component data which I felt was a more elegant solution since I wouldn't need to duplicate the state or refetch the data again. 
