import Vue from 'vue'

export const state = () => ({
  cartTotal: 0,
  cart: {},
  products: [],
  clientSecret: null,
  locales: ['en', 'es'],
  locale: 'en',
  error: null,
})

export const mutations = {
  SET_PRODUCTS(state, products) {
    state.products = products
  },
  CLEAR_CART(state) {
    state.cart = {}
    state.cartTotal = 0
  },
  REMOVE_ITEM(state, item) {
    state.cartTotal -= item.count
    Vue.delete(state.cart, item.id)
  },
  ADD_TO_CART(state, item) {
    state.cartTotal++
    if (item.id in state.cart) {
      state.cart[item.id].count++
    } else {
      const stateItem = Object.assign({}, item)
      stateItem.count = 1
      state.cart[item.id] = stateItem
    }
  },
  SET_CLIENT_SECRET(state, clientSecret) {
    state.clientSecret = clientSecret
  },
  SET_LANG(state, locale) {
    if (state.locales.includes(locale)) {
      state.locale = locale
    }
  },
  SET_ERROR(state, error) {
    state.error = error
  },
}

export const actions = {
  nuxtServerInit({ dispatch }, { req }) {
    return dispatch('getProducts')
  },
  async getProducts({ commit }) {
    try {
      const productsUrl = `${process.env.clientUrl}api/products`
      const {
        prices: { data },
      } = await this.$axios.$get(productsUrl)

      commit('SET_PRODUCTS', data)
    } catch (err) {
      commit('SET_ERROR', err)
    }
  },
  async createPaymentIntent({ getters, commit }) {
    try {

      const cart = getters.cart

      // prepare order items
      const items = Object.keys(cart).map((key, index) => {
        return {
          id: cart[key].id,
          title: cart[key].product.name,
          count: cart[key].count
        }
      })

      // Create a PaymentIntent with the information about the order
      const res = await this.$axios.$post(
        `${process.env.clientUrl}api/create-payment-intent`,
        {
          items,
        },
        {
          headers: {
            'Content-Type': 'application/json',
          },
        }
      )

      if (res.clientSecret) {
        // Store a reference to the client secret created by the PaymentIntent
        // we will use it to finalize the payment from the client
        commit('SET_CLIENT_SECRET', res.clientSecret)
      }
    } catch (err) {
      commit('SET_ERROR', err)
    }
  },
}

export const getters = {
  products: (state) => state.products,
  cart: (state) => state.cart,
  clientSecret: (state) => state.clientSecret,
  locales: (state) => state.locales
}
