const express = require('express')
const Stripe =  require('stripe')

// Ensure environment variables are set
const checkEnv = () => {
  const secret = process.env.STRIPE_API_SECRET
  if (!secret) {
    console.log(
      '🚨 Error! You must set STRIPE_API_SECRET in the environment variables. Please see the README.'
    )
    process.exit(0)
  }
}

checkEnv()

const stripe = new Stripe(process.env.STRIPE_API_SECRET)

const app = express()

/* To use 'req.body' -- to parse 'application/json' content-type */
app.use(express.json())

const asyncMiddleware = (fn) => (req, res, next) => {
  Promise.resolve(fn(req, res, next)).catch(next)
}

const calculateOrderAmount = async (items) => {

  const prices = await stripe.prices.list({
    expand: ['data.product'],
  })

  const amount = items.reduce((prev, item) => {
    // lookup item information from stripe and calculate total amount
    const itemData = prices.data.find(
      storeItem => storeItem.id === item.id
    );
    return prev + itemData.unit_amount * 100 * item.count;
  }, 0)

  return amount
}

/* create payment intent */
app.post('/create-payment-intent', async (req, res) => {
  const { items } = req.body

  if(!items || items.length < 1) {
    res.status(500).send({
      status: "missing information",
      message: "List of items to purchase is missing."
    })
  }

  const paymentTotal = await calculateOrderAmount(items)

  // Create a PaymentIntent with the order amount and currency
  const paymentIntent = await stripe.paymentIntents.create({
    amount: paymentTotal,
    currency: 'usd',
    description: `One time purchase of ${items.length} from StayStore`,
    // hardcoded but ideally would be filled by the user and sent to stripe.customers.create
    shipping: {
      name: 'Jenny Rosen',
      address: {
        line1: '510 Townsend St',
        postal_code: '98140',
        city: 'San Francisco',
        state: 'CA',
        country: 'US',
      },
    }
  })

  res.send({
    clientSecret: paymentIntent.client_secret,
  })
})

/* returns products list and prices */
app.get(
  '/products',
  asyncMiddleware(async (req, res, next) => {

    const prices = await stripe.prices.list({
      expand: ['data.product'],
    })

    res.send({
      prices
    })
  })
)

module.exports = {
  path: '/api/',
  handler: app,
}
