export default {
  // Disable server-side rendering (https://go.nuxtjs.dev/ssr-mode)
  ssr: false,

  // Global page headers (https://go.nuxtjs.dev/config-head)
  head: {
    title: 'staytion-checkout-client',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
    script: [
      { hid: 'stripe', src: 'https://js.stripe.com/v3/', defer: true }
    ]
  },

  // Global CSS (https://go.nuxtjs.dev/config-css)
  css: [{ src: '~assets/styles/buefy.scss', lang: 'scss' }],

  // Plugins to run before rendering page (https://go.nuxtjs.dev/config-plugins)
  plugins: ['~plugins/buefy','~/plugins/i18n.js'],

  // Auto import components (https://go.nuxtjs.dev/config-components)
  components: true,

  // Modules for dev and build (recommended) (https://go.nuxtjs.dev/config-modules)
  buildModules: [
    // https://go.nuxtjs.dev/eslint
    '@nuxtjs/eslint-module',
  ],

  // Modules (https://go.nuxtjs.dev/config-modules)
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    // https://go.nuxtjs.dev/pwa
    '@nuxtjs/pwa',
    '@nuxtjs/dotenv',
    '@nuxtjs/toast',
    'nuxt-fontawesome',
    '@nuxtjs/color-mode',
  ],

  // Enviromental Variables
  env: {
    stripeApiSecret: process.env.STRIPE_API_SECRET,
    stripeApiKey: process.env.STRIPE_API_KEY,
    clientUrl: process.env.CLIENT_URL,
  },

  // Build Configuration (https://go.nuxtjs.dev/config-build)
  build: {
    watch: ['api'],
  },

  axios: {
    baseURL: process.env.CLIENT_URL,
  },

  // add api directory to config so it registers when we start the server
  serverMiddleware: ['~/api'],

  router: {
    middleware: 'i18n'
  },
  toast: {
    position: 'top-right',
    duration: 4000,
    keepOnHover: true,
    theme: "bubble",
  },
  buefy: {
    materialDesignIcons: false,
    defaultIconPack: 'fas',
    defaultIconComponent: 'font-awesome-icon'
  },
  fontawesome: {
    imports: [
      {
        set: '@fortawesome/free-solid-svg-icons',
        icons: ['fas']
      }
    ]
  }
}
