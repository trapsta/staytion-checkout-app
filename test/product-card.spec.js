import Vuex from "vuex"
import { createLocalVue, mount } from '@vue/test-utils'
import ProductCard from '@/components/product-card.vue'

const localVue = createLocalVue()
localVue.use(Vuex)

const mutations = {
  ADD_TO_CART: jest.fn()
}

const store = new Vuex.Store({ mutations })

const mockProduct = {
  id: 'price_1HhyNNLmxBahnAwgGNMPfgQT',
  object: 'price',
  active: true,
  billing_scheme: 'per_unit',
  created: 1604067285,
  currency: 'usd',
  livemode: false,
  lookup_key: null,
  metadata: {},
  nickname: null,
  product: {
    id: 'prod_IIZWvosmWZchQY',
    object: 'product',
    active: true,
    attributes: [],
    created: 1604067285,
    description: 'Staytion mobile monthly service',
    images: ['https://files.stripe.com/links/fl_test_Y4nvGRgeBv8W97WFa0x62Yf5'],
    livemode: false,
    metadata: {},
    name: 'Staytion Mobile',
    statement_descriptor: null,
    type: 'service',
    unit_label: null,
    updated: 1604074833,
  },
  recurring: null,
  tiers_mode: null,
  transform_quantity: null,
  type: 'one_time',
  unit_amount: 999,
  unit_amount_decimal: '999',
}

describe('ProductCard', () => {
  test('commits the ADD_TO_CART mutation when add to cart is clicked', () => {
    const wrapper = mount(ProductCard, {
      store, localVue,
      propsData: {
        product: mockProduct,
      },
      stubs: ['i18n-n', 'font-awesome-icon', 'b-modal'],
      mocks: {
        $t: (msg) => msg,
        $toast: {
          success: (toast) => toast
        }
      },
    })

    wrapper.find('button').trigger('click');

    expect(mutations.ADD_TO_CART).toHaveBeenCalledWith(
      {},
      mockProduct
    )
  })
})
